```diff
- NOTE! This app is deprecated.
```

**Use [autovpn2](https://github.com/ruped24/autovpn2) instead.**

# AUTOVPNBOOK #

![](https://img.shields.io/badge/autovpnbook-python_2.7-blue.svg?style=flat-square) ![](https://img.shields.io/badge/dependencies-openvpn-orange.svg?style=flat-square) ![](https://img.shields.io/badge/GPL-v2-blue.svg?style=flat-square) 

## Automatically Login And Connect To VPNBook ##




***

** [Screenshot](https://drive.google.com/open?id=0B79r4wTVj-CZNzBfOVpaRnRQMHc) ** :wink:

## [Step 1](https://www.youtube.com/watch?time_continue=14&v=_IbPcyU_cpU): ##
* Download [autovpnbook repository](https://bitbucket.org/ruped24/autovpnbook/get/a8f77e45fe96.zip) and unzip.

* Download All [VPNBOOK](https://www.vpnbook.com/freevpn)'s Server OpenVPN Certificate Bundles. **See Step 2**.

## Step 2: ##
* The **get_vpnbook_bundle.sh** script will download, setup, auto-launch avpnb.sh, and AutoVPNBook Menu.

### Usage: ###
```
#!bash

sudo ./get_vpnbook_bundle.sh
```

### Usage: ###

* **avpnb.sh** will launch AutoVPNBook Menu.

```
#!bash

sudo ./avpnb.sh
```



## [**Anonymity Check**](http://proxydb.net/anon) ##

***

#### Disclaimer:

#### These scripts are written for educational purposes only!

#### We have donated to vpnbook.com. :thumbsup:

#### Legal Disclosure: 

#### We are not affiliated with vpnbook.com in any way.  We are not advocating the use of their or any [Free VPN](https://github.com/ruped24/autovpn2) service. ####