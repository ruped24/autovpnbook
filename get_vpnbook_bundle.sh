#! /bin/bash
#
# Written by Rupe
# Download and setup vpnbook OpenVpn Certificate Bundle v.1.1
#

spinner() {
  local i sp n
  sp='/-\|'
  n=${#sp}
  printf ' '
  while sleep 0.1
  do
    printf "%s\b" "${sp:i++%n:1}"
  done
}

install_deps() {
  clear
  echo ""
  printf "\n%1s\033[92mInstalling dependencies, please wait..."
  spinner &
  sudo apt-get update -q=2 &> /dev/null
  sudo apt-get -yq=2 install $1 $2 &> /dev/null
  kill %1
  printf '\x1b[H\x1b[2J'
  echo -e "\n\033[93m"
}

# Check for dependencies
declare -a deps=(openvpn python-pexpect)
[[ $(sudo which openvpn) ]] && \
[[ $(sudo dpkg --list | grep pexpect) ]]

# Install if missing dependencies
case $? in
  1) (install_deps ${deps[@]});;
  *) true;;
esac

cleanup() {
  rm VPN*.zip && rm $0 \
  && sudo bash avpnb.sh
}

trap cleanup Exit

dwnld_cert_bundle() {
  echo -e "\n\033[93m"
  wget -r -l1 -H -t1 -nd -N -np -A.zip -erobots=off \
  http://www.vpnbook.com/#openvpn
  return $?
}

unpack_bundle() {
  echo -e "\n\033[92m"
  for bundle in $(ls VPN*.zip)
  do
    unzip $bundle
  done
  return $?
}

dwnld_cert_bundle \
&& unpack_bundle

exit $?